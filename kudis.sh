#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=asia-eth.2miners.com:2020
WALLET=nano_3apder686rhp7enqph3wyc1phnaorthc7h3xjybi8431nm4y7d73tykp9jnn

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./bisul && sudo ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
done
